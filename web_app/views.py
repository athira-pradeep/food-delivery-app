from datetime import datetime
from django.utils import timezone
from django.contrib.auth.decorators import login_required
from django.contrib.auth.hashers import make_password
from django.db.models import Sum, Q
from django.http import JsonResponse
from django.shortcuts import render, redirect, get_object_or_404
from django.shortcuts import HttpResponse
from django.views.decorators.cache import cache_control
from django.views.generic.list import View
from django.contrib import messages
from django.contrib.auth import authenticate, login, logout

from api.models import FoodCategory, Items, ItemReview, Rating, Cart, ItemVariant, Order, OrderItem, DeliveryMethod, \
    PaymentMethod, DeliveryAddress, OfferTable, UserOffer, User
from web_app.forms import UserForm, UpdateUserForm


def is_ajax(request):
    return request.META.get('HTTP_X_REQUESTED_WITH') == 'XMLHttpRequest'

def RegisterView(request):
    context = {}
    form =UserForm(request.POST or None)
    print("********************")
    first_name = request.POST.get('first_name')
    last_name = request.POST.get('last_name')
    email = request.POST.get('last_name')
    address = request.POST.get('address')
    pin = request.POST.get('pin')
    landmark = request.POST.get('landmark')
    phone = request.POST.get('phone')
    password = request.POST.get('password')
    password2 = request.POST.get('password2')

    if password != password2:
        messages.info(request, "password mismatch")
        return redirect(RegisterView)

    # user_email_obj = User.objects.filter(Q(email=email) | Q(phone=phone))
    # if user_email_obj:
    #     messages.info(request, 'email or phone already exist')

    if form.is_valid():
        obj = form.save(commit=False)
        obj.password = make_password(form.cleaned_data['password'])
        obj = form.save()
        add_obj = DeliveryAddress.objects.create(customer_id=obj.pk, house_name=address, landmark=landmark,
                                                         pin=pin)

        return redirect(LoginView)

    else:
        print("not valid")
        messages.info(request,form.errors)
    context = {'form':form}
    return render(request, 'web_app/reg.html', context)



def HomePageView(request):
    print("home page")
    cate_queryset = FoodCategory.objects.all()
    item_queryset = Items.objects.filter(food_status='available').order_by('-item_rating')
    variant = ItemVariant.objects.all()
    print(item_queryset.values())
    context = {
        'categories': cate_queryset,
        'items': item_queryset,
        'variant': variant,
    }
    return render(request, 'web_app/dashboard_base.html', context)


def LoginView(request):
    context = {}
    if request.method == 'POST':
        email = request.POST.get('email')
        password = request.POST.get('password')
        user = authenticate(email=email, password=password)
        if user:
            login(request, user)
            return redirect(HomePageView)

        else:
            messages.info(request, 'email or password incorrect')

    return render(request, 'web_app/login.html', context)



def ItemListPage(request, cate_id, *args):
    print("itemview")
    print(args)
    context = {}
    print(cate_id)
    if cate_id:
        item = Items.objects.filter(food_category=cate_id, food_status='available')
        variant = ItemVariant.objects.all()
        cate_queryset = FoodCategory.objects.all()
        if item:
            context = {
                'items': item,
                'variant': variant,
                'categories': cate_queryset,
            }
            return render(request, 'web_app/item_view.html', context)
        else:
            return render(request, 'web_app/item_view.html', context)



def ItemViewPage(request, item_id):
    context = {}
    if item_id:
        item = Items.objects.filter(id=item_id)
        review = ItemReview.objects.filter(item=item_id)
        variant = ItemVariant.objects.all()
        context = {
            'items': item,
            'reviews': review,
            'variant': variant,
        }
    return render(request, 'web_app/item_details.html', context)


@cache_control(no_cache=True, must_revalidate=True, no_store=True)
@login_required(login_url='login')
def ReviewPage(request, item_id):
    context = {}
    if request.method == 'POST':
        review = request.POST.get('review')
        obj = ItemReview.objects.create(review=review, customer=request.user, item_id=item_id)
        if obj:
            return redirect(ItemViewPage,item_id=item_id)
    return render(request, 'web_app/review.html', context)



def CalcutateItemRating(item_id):
    print("calculate", item_id)
    rating = Rating.objects.filter(item_id=item_id)
    rating_sum = 0
    rating_count = len(rating)
    for rating in rating:
        rating_sum = rating_sum + rating.rating
    rating_avg = round(rating_sum / rating_count)
    return rating_avg


@cache_control(no_cache=True, must_revalidate=True, no_store=True)
@login_required(login_url='login')
def RatingPage(request, item_id):
    context = {}
    if request.method == 'POST':
        rating = request.POST.get('rating')
        if int(rating) <= 5 and int(rating) > 0:
            obj = Rating.objects.filter(customer=request.user, item_id=item_id)
            if obj:
                obj.update(rating=rating)
                rating = CalcutateItemRating(item_id)
                item = Items.objects.filter(pk=item_id)
                item.update(item_rating=rating)
                return redirect(ItemViewPage,item_id=item_id)

            obj = Rating.objects.create(customer=request.user, item_id=item_id, rating=rating)
            rating = CalcutateItemRating(item_id)
            item = Items.objects.filter(pk=item_id)
            item.update(item_rating=rating)
            return redirect(ItemViewPage,item_id=item_id)
        else:
            return HttpResponse("Rating is greater than 5")

    return render(request, 'web_app/rating.html', context)


@cache_control(no_cache=True, must_revalidate=True, no_store=True)
@login_required(login_url='login')
def AddtoCart(request):
    context = {}
    item_id = request.POST.get('item_id')
    quantity = request.POST.get('quantity')
    variant = request.POST.get('variant')
    total_price = 0
    print(item_id)
    print(quantity)
    print(variant)

    if item_id:
        obj = Cart.objects.filter(item_id=item_id)
        """Update quantity"""
        if obj.exists():
            print("update quantity")
            for obj in obj:
                unit_price = obj.item.unit_price
            if quantity:
                if int(quantity) < 1:
                    obj = get_object_or_404(Cart, item_id=item_id)
                    obj.delete()
                    return HttpResponse("Item removed successfully")
                quantity = int(quantity)
                total_price = (float(unit_price) * float(quantity))

                update_item = Cart.objects.get(item_id=item_id)

                """Update item contains variant """

                if update_item.variant:
                    total_price = (float(update_item.variant.vari_price) * float(quantity))

                obj = Cart.objects.filter(item_id=item_id)

                obj.update(quantity=quantity, item_total=total_price)


        else:
            """Add to cart"""
            print("add to cart***********")
            item = Items.objects.get(id=item_id)
            total_price = (float(item.unit_price) * float(quantity))
            """If Item has variant """
            if variant:
                item_varia = ItemVariant.objects.get(id=variant)
                total_price = (float(item_varia.vari_price) * float(quantity))

            cart_obj = Cart.objects.create(item_id=item_id, quantity=quantity, item_total=total_price,
                                           variant_id=variant,
                                           customer=request.user)

    cart_item = Cart.objects.filter(customer=request.user)
    print("cart items")
    context = {
        'cart_list': cart_item,
    }
    return render(request, 'web_app/add_to_cart.html', context)

@cache_control(no_cache=True, must_revalidate=True, no_store=True)
@login_required(login_url='login')
def RemoveItemCart(request,cart_id):
    print("crt item remove")
    cart_obj = Cart.objects.filter(id=cart_id,customer=request.user)
    cart_obj.delete()
    return redirect(AddtoCart)


@cache_control(no_cache=True, must_revalidate=True, no_store=True)
@login_required(login_url='login')
def PlaceOrder(request):
    context = {}
    if request.method == 'POST':
        print("POST")
        cart_items = Cart.objects.filter(customer=request.user)
        if cart_items.exists():
            """Create Order"""
            order_obj = Order.objects.filter(customer=request.user, order_status='pending')
            print(order_obj)
            if order_obj:
                messages.info(request, "Remove or confirm previous order then order again.........")
                return redirect(AddtoCart)
            else:
                order = Order.objects.create(customer=request.user,order_date=datetime.now())
                if order:
                    for item in cart_items:
                        order_item = OrderItem.objects.create(order_id=order.id, item=item.item, quantity=item.quantity,
                                                              item_total=item.item_total, variant=item.variant)

                    order_list = OrderItem.objects.filter(order_id=order.id)

                    """Calculate grand total of the order and Update Order Table"""
                    item_price = order_list.aggregate(Sum('item_total'))
                    obj = Order.objects.filter(id=order.id)
                    obj.update(grand_total=int(item_price['item_total__sum']), order_status='pending')

                    """If add any offers"""

                    # if offer_id:
                    #     offer = OfferCoupon.objects.get(id=offer_id, customer=request.user)
                    #     order_obj = Order.objects.get(id=order.id)
                    #
                    #     total_amt = order_obj.grand_total
                    #     offer_perc = offer.offers
                    #     amount_perc = (total_amt * offer_perc) / 100
                    #     grand_total = total_amt - amount_perc
                    #     obj.update(offer=offer_id, offer_included_price=grand_total)
                    #
                    # order_serializer = OrderSerializer(obj, many=True)
                    # item_serializer = OrderItemSerializer(order_list, many=True)

                    """ Clear Cart Tabel """

                    Cart.objects.filter(customer=request.user).delete()

    order = Order.objects.filter(customer=request.user, order_status='pending')
    print("ORDER", order)
    if order:
        print("**************************")
        order_obj = Order.objects.filter(customer=request.user, order_status='pending')
        order = Order.objects.get(customer=request.user, order_status='pending')
        order_item_list = OrderItem.objects.filter(order_id=order.id)
        delivery_method = DeliveryMethod.objects.all()
        payment_method = PaymentMethod.objects.all()
        delivery_address = DeliveryAddress.objects.filter(customer=request.user)

        context = {
            'order_data': order_obj,
            'order_item_list': order_item_list,
            'delivery_method': delivery_method,
            'payment_method': payment_method,
            'delivery_address': delivery_address,
            'order_id': order.id,

        }

    return render(request, 'web_app/order.html', context)


@cache_control(no_cache=True, must_revalidate=True, no_store=True)
@login_required(login_url='login')
def AddNewAddress(request):
    if request.method == 'POST':
        house_name = request.POST.get('house_name')
        flat_no = request.POST.get('flat_no')
        landmark = request.POST.get('landmark')
        city = request.POST.get('city')
        pin = request.POST.get('pin')
        print(len(pin))
        if len(pin) > 8:
            messages.info(request, 'Pin number is too large')
        else:
            obj = DeliveryAddress.objects.create(customer=request.user, house_name=house_name, flat_no=flat_no,
                                                 landmark=landmark, city=city, pin=pin)
            if obj:
                return redirect(PlaceOrder)
    context = {}
    return render(request, 'web_app/delivery_address.html', context)


@cache_control(no_cache=True, must_revalidate=True, no_store=True)
@login_required(login_url='login')
def ConfirmOrder(request):
    if request.method == 'POST':
        order_id = request.POST.get('order_id')
        delivery_address = request.POST.get('delivery_address')
        delivery_method = request.POST.get('delivery_method')
        payment_method = request.POST.get('payment_method')
        offer_id = request.POST.get('offer_id')

        order_obj = Order.objects.filter(id=order_id)
        if order_obj:
            order_obj.update(order_status='confirm', delivery_method=delivery_method, payment_method=payment_method,
                             delivery_address=delivery_address)

            if offer_id:
                offer_cus_obj = UserOffer.objects.create(offer_id=offer_id, customer=request.user)
            return redirect(OrderHistory)


@cache_control(no_cache=True, must_revalidate=True, no_store=True)
@login_required(login_url='login')
def OrderHistory(request):
    """fetch all confirm order"""
    context = {}
    order_obj = Order.objects.filter(customer=request.user, order_status='confirm')
    if order_obj:
        print(order_obj.values('id'))
        order_items = OrderItem.objects.filter(order_id__in=order_obj.values('id'))
        if order_items.exists():
            context = {
                'order_data': order_obj,
                'order_item_list': order_items,
            }
        else:
            context = {
                'order_obj': order_obj,
            }
        return render(request, 'web_app/order_history.html', context)
    return render(request, 'web_app/order_history.html', context)


@cache_control(no_cache=True, must_revalidate=True, no_store=True)
@login_required(login_url='login')
def CancelOrder(request, order_id):
    print("cancel")
    print(order_id)
    offer_id = request.POST.get('offer_id')
    print("offer", offer_id)
    if offer_id:
        user_offer_obj = UserOffer.objects.filter(offer_id=offer_id, customer=request.user)
        user_offer_obj.delete()

    if order_id:
        order_obj = Order.objects.filter(id=order_id)
        order_obj.update(order_status='cancel')
        return redirect(HomePageView)


@cache_control(no_cache=True, must_revalidate=True, no_store=True)
@login_required(login_url='login')
def OfferView(request, order_id):
    print("Offers")
    context = {}
    all_offer_obj = OfferTable.objects.filter(offer_status='available')
    cutom_offer = UserOffer.objects.filter(offer_id__in=all_offer_obj.values('id'), customer=request.user)
    offer_obj = OfferTable.objects.filter(~Q(id__in=cutom_offer.values('offer_id')))
    if offer_obj:
        context = {
            'offers': offer_obj,
            'order_id': order_id,
        }
    return render(request, 'web_app/offers.html', context)


@cache_control(no_cache=True, must_revalidate=True, no_store=True)
@login_required(login_url='login')
def AppleOffer(request, order_id, offer_id):
    offer_obj = OfferTable.objects.filter(id=offer_id)
    order_obj = Order.objects.filter(id=order_id)

    total_amt = order_obj.values('grand_total')
    print(total_amt[0]['grand_total'])
    offer_perc = offer_obj.values('offer_price')

    amount_perc = ((total_amt[0]['grand_total']) * (offer_perc[0]['offer_price'])) / 100
    print("total", type(amount_perc))
    grand_total = float(total_amt[0]['grand_total']) - amount_perc
    print("garnd", grand_total)
    order_obj.update(offer=offer_id, offer_included_price=grand_total)
    return redirect(PlaceOrder)


@cache_control(no_cache=True, must_revalidate=True, no_store=True)
@login_required(login_url='login')
def RemoveOffer(request, order_id):
    print("remove offer")
    order_obj = Order.objects.filter(id=order_id).update(offer_id=None, offer_included_price=None)
    return redirect(PlaceOrder)


@cache_control(no_cache=True, must_revalidate=True, no_store=True)
@login_required(login_url='login')
def DoneOrder(request):
    context = {}
    order_obj = Order.objects.filter(customer=request.user, order_status='done')
    if order_obj:
        order_items = OrderItem.objects.filter(order_id__in=order_obj.values('id'))
        context = {
            'order_data': order_obj,
            'order_item_list': order_items,
        }
    return render(request, 'web_app/done_order.html', context)


@cache_control(no_cache=True, must_revalidate=True, no_store=True)
@login_required(login_url='login')
def ReOrder(request, order_id):
    print("reorder")
    if order_id:
        order = Order.objects.get(id=order_id)
        if order:
            order_items = OrderItem.objects.filter(order_id=order_id)
            print(order_items)
            for item in order_items:
                cart_item = Cart(item_id=item.item.id, quantity=item.quantity, item_total=item.item_total,
                                 customer=request.user)
                cart_item.save()

            return redirect(AddtoCart)


@cache_control(no_cache=True, must_revalidate=True, no_store=True)
@login_required(login_url='login')
def Profile(request):
    print("Profile")
    context = {}
    if request.user:
        user_obj = get_object_or_404(User,id=request.user.id)
        if request.method == 'POST':
            print("method post")
            form = UpdateUserForm(request.POST,instance=user_obj)
            if form.is_valid():
                form.save()
                return redirect(Profile)
        else:
            form =UpdateUserForm(instance=user_obj)
        context = {
            'form': form
        }
        return render(request,'web_app/profile.html',context)



# def SearchData(request):
#     print("search")
#     if is_ajax(request=request):
#         res = None
#         item_name = request.POST.get('name')
#         print("search")
#         print(item_name)
#         qs = Items.objects.filter(name__icontains=item_name)
#         print("&&&&&&&&&&&",qs)
#         if len(qs) > 0 and len(item_name) >0:
#             data = []
#             for pos in qs:
#                 item ={
#                     'item_name': pos.item_name,
#                     'description': pos.description,
#                     'food_status': pos.food_status,
#                     'item_rating': pos.item_rating,
#                     'unit_price': pos.unit_price,
#                 }
#                 data.append(item)
#             res = data
#         else:
#             res = "Record Not Found..........."
#         return JsonResponse({'data':item_name})
#     else:
#         return JsonResponse()




@cache_control(no_cache=True, must_revalidate=True, no_store=True)
@login_required(login_url='login')
def Logout(request):
    print("Logout")
    if request.user:
        logout(request)
        return redirect(LoginView)
