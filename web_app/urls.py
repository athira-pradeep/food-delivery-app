from django.urls import path
from . import views



urlpatterns = [
    path('registration/',views.RegisterView,name='registration'),
    path('login/',views.LoginView,name='login'),
    path('home/',views.HomePageView,name='home'),
    path('item_list/<int:cate_id>/',views.ItemListPage,name='item_view'),
    path('item_details/<int:item_id>/',views.ItemViewPage,name='detailed_view'),
    path('add-review/<int:item_id>/',views.ReviewPage,name='add_review'),
    path('add_rating/<int:item_id>/',views.RatingPage,name='add_rating'),
    path('add_to_cart/',views.AddtoCart,name='add_to_cart'),
    path('remove-item-cart/<int:cart_id>/',views.RemoveItemCart,name='remove_item_cart'),
    path('order/',views.PlaceOrder,name='place_order'),
    path('add-delivery-address/',views.AddNewAddress,name='add_new_address'),
    path('confirm-order/',views.ConfirmOrder,name='confirm_order'),
    path('offer-page/<int:order_id>/',views.OfferView,name='offer_page'),
    path('apply-offer/<int:order_id>/<int:offer_id>/',views.AppleOffer,name='apply_offer'),
    path('remove-offer/<int:order_id>/',views.RemoveOffer,name='remove_offer'),
    path('order_history/',views.OrderHistory,name='order_history'),
    path('cancel_order/<int:order_id>/',views.CancelOrder,name='cancel_order'),
    path('done-order/',views.DoneOrder,name='done_order'),
    path('re-order/<int:order_id>/',views.ReOrder,name='re_order'),
    path('profile/',views.Profile,name='profile'),
    # path('search_data/',views.SearchData,name='search_data'),
    path('logout/',views.Logout,name='logout'),

]