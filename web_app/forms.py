from django.forms import ModelForm
from phonenumber_field import validators

from api.models import User
from django import forms

class UserForm(ModelForm):
    email = forms.EmailField(widget= forms.TextInput
                           (attrs={'class':'form-control'}))
    phone = forms.CharField(widget= forms.TextInput
                           (attrs={'class':'form-control'}))
    password = forms.CharField(widget= forms.PasswordInput
                           (attrs={'class':'form-control'}))
    first_name = forms.CharField(widget= forms.TextInput
                           (attrs={'class':'form-control'}))
    last_name = forms.CharField(widget= forms.TextInput
                           (attrs={'class':'form-control'}))
    address = forms.CharField(widget= forms.TextInput
                           (attrs={'class':'form-control'}))
    pin = forms.IntegerField(widget= forms.NumberInput
                           (attrs={'class':'form-control'}))
    landmark = forms.CharField(widget= forms.TextInput
                           (attrs={'class':'form-control'}))

    class Meta:
        model = User
        fields = ('email','phone','password','first_name','last_name','address','pin','landmark')



class UpdateUserForm(ModelForm):
    email = forms.EmailField(widget= forms.TextInput
                           (attrs={'class':'form-control'}))
    phone = forms.CharField(widget= forms.TextInput
                           (attrs={'class':'form-control'}))
    first_name = forms.CharField(widget= forms.TextInput
                           (attrs={'class':'form-control'}))
    last_name = forms.CharField(widget= forms.TextInput
                           (attrs={'class':'form-control'}))
    address = forms.CharField(widget= forms.TextInput
                           (attrs={'class':'form-control'}))
    pin = forms.IntegerField(widget= forms.NumberInput
                           (attrs={'class':'form-control'}))
    landmark = forms.CharField(widget= forms.TextInput
                           (attrs={'class':'form-control'}))

    class Meta:
        model = User
        fields = ('email','phone','first_name','last_name','address','pin','landmark')