from django.contrib import admin
from .models import User,FoodType,FoodCategory,Image,Items,DeliveryMethod,ItemReview,Rating,Cart,OrderItem,Order,Offers,FAQ,PaymentMethod,OfferCoupon,ItemVariant,\
                    DeliveryAddress,OfferTable,UserOffer
# Register your models here.

admin.site.register(User)
admin.site.register(FoodCategory)
admin.site.register(FoodType)
admin.site.register(Image)
admin.site.register(Items)
admin.site.register(DeliveryMethod)
admin.site.register(ItemReview)
admin.site.register(Rating)
admin.site.register(Cart)
admin.site.register(Order)
admin.site.register(OrderItem)
admin.site.register(Offers)
admin.site.register(FAQ)
admin.site.register(PaymentMethod)
admin.site.register(OfferCoupon)
admin.site.register(ItemVariant)
admin.site.register(DeliveryAddress)
admin.site.register(OfferTable)
admin.site.register(UserOffer)
