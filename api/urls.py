from django.urls import path,include
from api.views import SignUpApi, SignInApi, UserRetrieveUpdateAPIView, ItemApiViews, CategoryApiView, FoodTypeApiView, \
    FoodReviewApiView, FoodRatingView, CartApiView, OrderApiView, DeliveryMethodApiView, SignOutApiView, OffersApiView, \
    FAQView, PaymentMethodView, UserCouponView, DeliveryAddressView

urlpatterns = [
    path('api/userviewset/',SignUpApi.as_view(),name='userviewset'),
    path('api/auth/login/',SignInApi.as_view(),name='login'),
    path('api/auth/logout/',SignOutApiView.as_view({'get':'get'}),name='signout'),

    path('api/auth/user-retrieve-update/',UserRetrieveUpdateAPIView.as_view(),name='user-retrieve-update'),

    path('api/auth/food-type/',FoodTypeApiView.as_view({'get':'list'}),name='food-types'),
    path('api/auth/food-category/',CategoryApiView.as_view({'get':'list'}),name='food-category'),

    path('api/auth/foodview-set/',ItemApiViews.as_view({'get':'get'}),name='food-viewset'),
    path('api/auth/food-review/',FoodReviewApiView.as_view({'post':'post','get':'get'}),name='food-review'),
    path('api/auth/food-rating/',FoodRatingView.as_view({'post':'post'}),name='food-rating'),

    path('api/auth/add-to-cart/',CartApiView.as_view({'post':'post','get':'get'}),name='add-to-cart'),

    path('api/auth/confirm-order/',OrderApiView.as_view({'post':'post','get':'get'}),name='confirm-order'),
    path('api/auth/re-order/',OrderApiView.as_view({'post':'re_order'}),name='re-order'),

    path('api/auth/list-delivery-method/',DeliveryMethodApiView.as_view({'get':'get'}),name='delivery-method'),

    path('api/auth/offers/',OffersApiView.as_view({'get':'get'}),name='offers'),
    path('api/auth/faq/',FAQView.as_view({'get':'get'}),name='faq'),
    path('api/auth/payment-method/',PaymentMethodView.as_view({'get':'get'}),name='payment-method'),

    path('api/auth/coupon/',UserCouponView.as_view({'get':'get'}),name='coupon'),

    path('api/auth/add-new-address/',DeliveryAddressView.as_view({'post':'post'}),name='add-new-address'),


]