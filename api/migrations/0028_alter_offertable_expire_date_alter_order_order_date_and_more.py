# Generated by Django 4.1 on 2022-09-29 06:23

import datetime
from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('api', '0027_offertable_expire_date_alter_order_order_date'),
    ]

    operations = [
        migrations.AlterField(
            model_name='offertable',
            name='expire_date',
            field=models.DateField(blank=True, default=datetime.datetime(2022, 9, 29, 6, 23, 8, 698526), null=True),
        ),
        migrations.AlterField(
            model_name='order',
            name='order_date',
            field=models.DateTimeField(default=datetime.datetime(2022, 9, 29, 6, 23, 8, 697079)),
        ),
        migrations.CreateModel(
            name='UserOffer',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('customer', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL)),
                ('offer', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='api.offertable')),
            ],
        ),
    ]
