# Generated by Django 4.1 on 2022-09-30 05:55

import datetime
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('api', '0033_alter_offertable_expire_date_alter_order_order_date'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='items',
            name='image',
        ),
        migrations.AlterField(
            model_name='offertable',
            name='expire_date',
            field=models.DateField(blank=True, default=datetime.datetime(2022, 9, 30, 5, 55, 37, 604671), null=True),
        ),
        migrations.AlterField(
            model_name='order',
            name='order_date',
            field=models.DateTimeField(default=datetime.datetime(2022, 9, 30, 5, 55, 37, 605259)),
        ),
    ]
