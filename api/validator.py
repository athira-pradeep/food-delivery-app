from phonenumber_field.phonenumber import to_python
from phonenumber_field.phonenumber import to_python
from phonenumbers.phonenumberutil import is_possible_number
from django.core.exceptions import ValidationError


def validate_possible_number(phone_number,country=None):
    phone_number = to_python(phone_number,country)
    if (
        phone_number
        and not is_possible_number(phone_number)
        or not phone_number.is_valid()
        or len(phone_number) < 10
    ):
        raise ValidationError("The phone number is invalid", code = 'invalid')

    return phone_number
