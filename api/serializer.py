import uuid
from datetime import datetime

from rest_framework import serializers
from rest_framework.permissions import IsAuthenticated
from django.db import models
# from django.contrib.auth.models import User
from django.contrib.auth import authenticate
from django.contrib.auth.hashers import make_password
from rest_framework.serializers import ModelSerializer

from api.models import User, Image, Items, FoodCategory, FoodType, ItemReview, Rating, Cart, Order, OrderItem, \
    DeliveryMethod, Offers, FAQ, PaymentMethod, OfferCoupon, ItemVariant, DeliveryAddress
from rest_framework.validators import UniqueValidator
from versatileimagefield.serializers import VersatileImageFieldSerializer
from django.core.validators import MaxValueValidator, MinValueValidator
from rest_flex_fields import FlexFieldsModelSerializer
from django.utils.translation import gettext as _


def field_length(fieldname):
    field = next(field for field in User._meta.fields if field.name == fieldname)
    return field.max_length


# Register serializer

class RegisterSerializer(serializers.ModelSerializer):
    email = serializers.EmailField(validators=[UniqueValidator(queryset=User.objects.all())])
    phone = serializers.CharField(max_length=field_length("phone"), required=True, validators=[
        UniqueValidator(queryset=User.objects.all(), message=_("Mobile already exist with another User."))])
    password = serializers.CharField(max_length=50)
    password2 = serializers.CharField(max_length=50)
    first_name = serializers.CharField(max_length=220)
    last_name = serializers.CharField(max_length=220)
    address = serializers.CharField(max_length=500)
    pin = serializers.IntegerField()
    landmark = serializers.CharField(max_length=220)


    class Meta:
        model = User
        fields = ('password','password2' ,'email', 'phone', 'first_name', 'last_name', 'address', 'pin', 'landmark')
        extra_kwargs = {'password': {'write_only': True}}

    def validate(self, data):
        password = data['password']
        password2 = data['password2']
        if password != password2:
            raise serializers.ValidationError({
                "message": "Password Mismatch"
            })
        response={}
        try:
            User.objects.get(email=data['email'], phone=data['phone'])
            response['message'] = _(
                "User is already exist with the same email or phone number, please register with new email or phone number")
        except User.MultipleObjectsReturned:
            response['message'] = _("User is already exist")
        except:
            response["message"] = _("Email or phone number not available / invalid")

        return data

    def create(self, validated_data):
        user = User.objects.create_user(password=validated_data['password'],
                                        email=validated_data['email'], phone=str(validated_data['phone']),
                                        first_name=validated_data['first_name'], last_name=validated_data['last_name'],
                                        address=validated_data['address'], pin=validated_data['pin'],
                                        landmark=validated_data['landmark'])
        return user  # User serializer


class UserSerializer(serializers.ModelSerializer):

    class Meta:
        model = User
        fields = '__all__'


class LoginSerializer(serializers.ModelSerializer):
    email = serializers.EmailField(required=True)
    password = serializers.CharField(required=True, style={"input_type": "password"}, )

    class Meta:
        model = User
        fields = ('email', 'password')

    def validate(self, attrs):
        response = {}
        try:
            user_is = User.objects.get(email=attrs["email"])
        except User.DoesNotExist:
            response['email'] = 'Email address not found'

        if response.keys():
            raise serializers.ValidationError(response)

        user = authenticate(username=user_is.email, password=attrs["password"])
        if user is None:
            if user_is.is_active == False:
                response = {'message': 'User not found / Your account blocked please send enquiry to support'}
            else:
                response = {'password': 'Invalid password'}
        elif not user.is_active:
            response = {'message': 'Your account is inactive please send enquiry to support'}

        if response.keys():
            raise serializers.ValidationError(response)

        self.instance = user
        return attrs


class ImageSerializer(serializers.ModelSerializer):
    image = VersatileImageFieldSerializer(
        sizes=[
            ('full_size', 'url'),
            ('thumbnail', 'thumbnail__100x100'),
        ]
    )

    class Meta:
        model = Image
        fields = ['name', 'image']


class FoodTypeSerializer(serializers.ModelSerializer):
    food_type = serializers.CharField(max_length=500)

    class Meta:
        model = FoodType
        fields = '__all__'


class FoodCategorySerializer(serializers.ModelSerializer):
    food_category = models.CharField(max_length=500)

    class Meta:
        model = FoodCategory
        fields = '__all__'


FOOD_STATUS = (
    ('available', 'available'),
    ('not-available', 'not available'),
)


class ItemSerializer(ModelSerializer):
    name = serializers.CharField(max_length=300)
    food_type = FoodTypeSerializer(required=False, read_only=True)
    food_category = FoodCategorySerializer(required=False, read_only=True)
    image = ImageSerializer(required=False, read_only=True, many=True)
    slug = serializers.SlugField()
    description = serializers.CharField(max_length=599)
    unit_price = serializers.FloatField()
    quantity = serializers.IntegerField()
    item_rating = serializers.IntegerField()
    food_status = serializers.ChoiceField(choices=FOOD_STATUS)
    offer_price = serializers.FloatField()

    class Meta:
        model = Items
        fields = (
        'id', 'name', 'slug', 'description', 'unit_price', 'quantity', 'food_status', 'food_type', 'item_rating',
        'food_category', 'image','offer_price')



class ItemVariantSerializer(serializers.ModelSerializer):
    item = ItemSerializer(required=False,read_only=True)
    variant = serializers.CharField(max_length=100)
    vari_price = serializers.FloatField()

    class Meta:
        model = ItemVariant
        fields = '__all__'




class ItemReviewSerializer(serializers.ModelSerializer):
    review = serializers.CharField(max_length=500)
    customer = UserSerializer()
    item = ItemSerializer()

    class Meta:
        model = ItemReview
        fields = '__all__'


class ItemRatingSerializer(serializers.ModelSerializer):
    rating = serializers.IntegerField(
        validators=[
            MaxValueValidator(5),
            MinValueValidator(1),
        ]
    )
    item = ItemSerializer(required=False, read_only=True)
    customer = UserSerializer(required=False, read_only=True)

    class Meta:
        model = Rating
        fields = '__all__'




class OfferCouponSerializer(serializers.ModelSerializer):
    customer = UserSerializer()
    offers = serializers.IntegerField()

    class Meta:
        model = OfferCoupon
        fields = '__all__'






class CartSerializer(serializers.ModelSerializer):
    item = ItemSerializer(required=False, read_only=True)
    quantity = serializers.IntegerField()
    item_total = serializers.FloatField()
    variant = ItemVariantSerializer(required=False,read_only=True)

    class Meta:
        model = Cart
        fields = '__all__'


ORDER_STATUS = (
    ('pending', 'pending'),
    ('confirm', 'confirm'),
    ('done', 'done'),
)
PAYMENT_STATUS = (
    ('pending', 'pending'),
    ('confirm', 'confirm'),
)




class PaymentMethodSerializer(serializers.ModelSerializer):
    payment_method = serializers.CharField(max_length=500)

    class Meta:
        model = PaymentMethod
        fields = '__all__'


class DeliveryMethodSerializer(serializers.ModelSerializer):
    delivery_method = serializers.CharField(max_length=300)

    class Meta:
        model = DeliveryMethod
        fields = '__all__'







class OrderSerializer(serializers.ModelSerializer):
    order_number = serializers.UUIDField(read_only=True)
    grand_total = serializers.FloatField(default=0, read_only=True)
    offer_included_price = serializers.FloatField()
    offer = OfferCouponSerializer(required=False, read_only=True)
    order_status = serializers.ChoiceField(choices=ORDER_STATUS, default='pending')
    payment_status = serializers.ChoiceField(choices=PAYMENT_STATUS, default='pending')
    order_date = serializers.DateTimeField(default=datetime.now())

    payment_method = PaymentMethodSerializer(required=False, read_only=True)
    delivery_method = DeliveryMethodSerializer(required=False, read_only=True)
    customer = UserSerializer(required=False, read_only=True)



    class Meta:
        model = Order
        fields = '__all__'


class OrderItemSerializer(serializers.ModelSerializer):
    order = OrderSerializer(required=False, read_only=True)
    item = ItemSerializer(required=False, read_only=True)
    variant = ItemVariantSerializer(required=False,read_only=True)
    quantity = serializers.IntegerField()
    item_total = serializers.FloatField()

    class Meta:
        model = OrderItem
        fields = '__all__'




class SignOutSerializer(serializers.Serializer):
    refresh_token = serializers.CharField(required=True)



class OfferSerializer(serializers.ModelSerializer):
    offer = serializers.IntegerField()
    item = ItemSerializer(required=False,read_only=True)

    class Meta:
        model = Offers
        fields = ('offer','item')

    def __str__(self):
        return self.item.name


class FAQAserializer(serializers.ModelSerializer):
    qus = serializers.CharField(max_length=1000)
    ans = serializers.CharField(max_length=1000)

    class Meta:
        model = FAQ
        fields = ('qus','ans')




class DeliveryAddressSerializer(serializers.ModelSerializer):
    customer = UserSerializer
    house_name = serializers.CharField(max_length=1000)
    flat_no = serializers.CharField(max_length=500)
    landmark = serializers.CharField(max_length=500)
    pin = serializers.IntegerField()
    city = serializers.CharField(max_length=600)

    class Meta:
        model = DeliveryAddress
        fields = '__all__'




