import pdb
import datetime
from django.utils import timezone

from django.contrib.auth import login, logout
from django.contrib.auth.decorators import login_required
from django.http import Http404
from django.shortcuts import render
from django.db.models import Q, Sum
from django.views.decorators.csrf import csrf_exempt
from rest_framework import generics, permissions, mixins, status, filters
from rest_framework.generics import RetrieveUpdateAPIView, get_object_or_404, GenericAPIView
from rest_framework.views import APIView
from rest_framework.response import Response
from .serializer import UserSerializer, LoginSerializer, ItemSerializer, ImageSerializer, \
    FoodTypeSerializer, FoodCategorySerializer, ItemReviewSerializer, ItemRatingSerializer, CartSerializer, \
    OrderSerializer, OrderItemSerializer, DeliveryMethodSerializer, SignOutSerializer, RegisterSerializer, \
    OfferSerializer, FAQAserializer, PaymentMethodSerializer, OfferCouponSerializer, ItemVariantSerializer, \
    DeliveryAddressSerializer
from django.contrib.auth.models import User, update_last_login
from rest_framework import viewsets
from api.utils import GenerateOauthToken, OauthClientIDANDSecret
from rest_framework.permissions import IsAuthenticated, AllowAny
from rest_framework.decorators import permission_classes
from .models import User, Items, FoodType, FoodCategory, Image, ItemReview, Rating, Cart, Order, OrderItem, \
    DeliveryMethod, Offers, FAQ, PaymentMethod, OfferCoupon, ItemVariant, DeliveryAddress
from django_filters import rest_framework as filters
from rest_framework.filters import SearchFilter
from oauth2_provider.models import AccessToken, RefreshToken

AUTH2_APPLICATION_ID = 1  # OAuth2 application id


# Register API
class SignUpApi(generics.GenericAPIView):
    serializer_class = RegisterSerializer

    @permission_classes([AllowAny])
    def post(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        if serializer.is_valid(raise_exception=True):
            user = serializer.save()
            return Response({
                "user": UserSerializer(user, context=self.get_serializer_context()).data,
                "message": "User Created Successfully.  Now perform Login to get your token",
            })


# Login Api
class SignInApi(APIView):
    serializer_class = LoginSerializer

    @permission_classes([AllowAny])
    def post(self, request, *args, **kwargs):
        if self.request.user.is_authenticated:
            return Response({"result": "failure", "message": "User session is in use, Logout first"},
                            status=status.HTTP_405_METHOD_NOT_ALLOWED)

        user_is = self.serializer_class(data=request.data, context={'request': request})

        if user_is.is_valid():
            user = user_is.instance

            response_data = {}
            client_app = OauthClientIDANDSecret(AUTH2_APPLICATION_ID)
            response_data['token'] = GenerateOauthToken(request, user, client_app['client_id'])
            login(request, user)
            response = Response(response_data, status=status.HTTP_200_OK)

            if response.status_code == 200:
                response = {
                    'result': 'success',
                    'token': response_data['token'],
                    'id': user.id,
                    'email': user.email,
                    'phone': str(user.phone),
                }
                update_last_login(None, user)
            else:
                response = {"result": "failure", "message": "token authentication errors"}

        else:
            response = {'result': 'failure', 'errors': {i: user_is.errors[i][0] for i in user_is.errors.keys()}}
        return Response(response, status=status.HTTP_200_OK)


# View and update logged user details
class UserRetrieveUpdateAPIView(RetrieveUpdateAPIView):
    permission_classes = (IsAuthenticated,)
    serializer_class = UserSerializer

    def retrieve(self, request, *args, **kwargs):
        print("get")
        serializer = self.serializer_class(request.user)
        return Response(serializer.data, status=status.HTTP_200_OK)

    def update(self, request, *args, **kwargs):
        print("post")
        serializer = self.serializer_class(request.user, data=request.data, partial=True)
        serializer.is_valid(raise_exception=True)
        serializer.save()

        return Response(serializer.data, status=status.HTTP_200_OK)


# View & search Items
class ItemApiViews(viewsets.ModelViewSet):
    # permission_classes = (IsAuthenticated,)
    serializer_class = ItemSerializer
    queryset = Items.objects.all()

    @permission_classes([AllowAny])
    def get(self, request, *args, **kwargs):
        cate_id = request.GET.get('cate_id')
        type_id = request.GET.get('type_id')
        pk = request.GET.get('pk')
        condition = {}
        name = request.GET.get('name')
        slug = request.GET.get('slug')
        if name or slug:
            if name:
                condition['name__icontains'] = name
            if slug:
                condition['slug__icontains'] = slug
            item = Items.objects.filter(**condition)

            if item:
                serializer = ItemSerializer(item, many=True)
                return Response({'status': 'success', 'data': serializer.data}, status.HTTP_200_OK)
            return Response({'status': 'Not Found'}, status.HTTP_404_NOT_FOUND)

        # if cate_id:
        #     condition['food_category_id'] = cate_id
        # elif type_id:
        #     condition['food_type_id'] = type_id
        #
        # item = Items.objects.filter(**condition)
        #
        # if item:
        #     serializer = ItemSerializer(item, many=True)
        #     return Response({'status': 'success', 'data': serializer.data}, status.HTTP_200_OK)
        # return Response({'status': 'Not Found'}, status.HTTP_404_NOT_FOUND)

        # elif pk:
        #     condition['pk'] = pk
        #
        # item = Items.objects.filter(**condition)
        #
        # if item:
        #     item_varia = ItemVariant.objects.filter(item_id=item.id)
        #     if item_varia:
        #         varia_ser = ItemVariantSerializer(item_varia, many=True)
        #     serializer = ItemSerializer(item)
        #     return Response({'status': 'success', 'data': serializer.data, 'item-variant': varia_ser.data},
        #                     status.HTTP_200_OK)
        # else:
        #     return Response({'status': 'Not Found'}, status.HTTP_404_NOT_FOUND)

        if cate_id:
            item = Items.objects.filter(food_category=cate_id)
            if item:
                serializer = ItemSerializer(item, many=True)
                return Response({'status': 'success', 'data': serializer.data}, status=status.HTTP_200_OK)
            return Response({'status': 'Not found'}, status.HTTP_404_NOT_FOUND)
        elif type_id:
            item = Items.objects.filter(food_type=type_id)
            if item:
                serializer = ItemSerializer(item, many=True)
                return Response({'status': 'success', 'data': serializer.data}, status.HTTP_200_OK)
            return Response({'status': 'Not Found'}, status.HTTP_404_NOT_FOUND)
        if pk:
            print("**************", pk)
            item = Items.objects.get(pk=pk)
            if item:
                item_varia = ItemVariant.objects.filter(item_id=item.id)
                if item_varia:
                    varia_ser = ItemVariantSerializer(item_varia, many=True)
                serializer = ItemSerializer(item)
                return Response({'status': 'success', 'data': serializer.data, 'item-variant': varia_ser.data},
                                status.HTTP_200_OK)
            return Response({'status': 'Not Found'}, status.HTTP_404_NOT_FOUND)

        queryset = Items.objects.all()
        serializer = ItemSerializer(queryset, many=True)
        return Response({'status': 'success', 'data': serializer.data}, status=status.HTTP_200_OK)


# List food category
class CategoryApiView(viewsets.ModelViewSet):
    # permission_classes = (IsAuthenticated,)
    serializer_class = FoodCategorySerializer
    queryset = FoodCategory.objects.all()

    @permission_classes([AllowAny])
    def list(self, request, *args, **kwargs):
        queryset = FoodCategory.objects.all()
        serializer = FoodCategorySerializer(queryset, many=True)
        return Response(serializer.data)


# List food type
class FoodTypeApiView(viewsets.ModelViewSet):
    serializer_class = FoodTypeSerializer
    queryset = FoodType.objects.all()

    @permission_classes([AllowAny])
    def list(self, request, *args, **kwargs):
        serializer = self.serializer_class(self.queryset, many=True)
        return Response(serializer.data)


# Post & List review
class FoodReviewApiView(viewsets.ModelViewSet):
    permission_classes = (IsAuthenticated,)
    queryset = ItemReview.objects.all()
    serializer_class = ItemReviewSerializer

    def post(self, request):
        review = request.POST.get('review')
        customer = request.user
        item_id = request.POST.get('item')
        review = ItemReview.objects.create(
            review=review,
            customer=customer,
            item_id=item_id,
        )

        serializer = self.serializer_class(review)

        return Response({"message": "Review posted successfully", "data": serializer.data}, status.HTTP_200_OK)

    def get(self, request, *args, **kwargs):
        item_id = request.GET.get('item_id')
        if item_id:
            item = ItemReview.objects.filter(item_id=item_id)
            if item:
                serializer = self.serializer_class(item, many=True)
                return Response({'status': 'success', 'data': serializer.data}, status.HTTP_200_OK)
            return Response({'status': 'Not Found'}, status.HTTP_404_NOT_FOUND)


"""Post,update & view Rating"""


class FoodRatingView(viewsets.ModelViewSet):
    permission_classes = (IsAuthenticated,)
    queryset = Rating.objects.all()
    serializer_class = ItemRatingSerializer

    def CalcutateItemRating(self, item_id):
        rating = Rating.objects.filter(item_id=item_id)
        rating_sum = 0
        rating_count = len(rating)
        for rating in rating:
            rating_sum = rating_sum + rating.rating
        rating_avg = round(rating_sum / rating_count)
        return rating_avg

    def post(self, request):

        food = User.objects.filter(pk=request.user.id, email=request.user.email, phone=request.user.phone,
                                   first_name=request.user.first_name, last_name=request.user.last_name)

        rating = request.POST.get('rating')
        item_id = request.POST.get('item')
        user = request.user
        if int(rating) <= 5 and int(rating) > 0:
            obj = Rating.objects.filter(customer=user, item_id=item_id)
            if obj:
                obj.update(rating=rating)
                serializer = self.serializer_class(obj, many=True)
                rating = self.CalcutateItemRating(item_id)
                item = Items.objects.filter(pk=item_id)
                item.update(item_rating=rating)
                return Response({'status': 'success', 'data': serializer.data}, status.HTTP_200_OK)

            obj = Rating.objects.create(customer=user, item_id=item_id, rating=rating)
            serializer = self.serializer_class(obj)
            rating = self.CalcutateItemRating(item_id)
            item = Items.objects.filter(pk=item_id)
            item.update(item_rating=rating)
            return Response({'status': 'success', 'data': serializer.data}, status.HTTP_200_OK)
        else:
            return Response({'status': 'Rating is greater than 5'}, status.HTTP_400_BAD_REQUEST)


# aAdd to cart
class CartApiView(viewsets.ModelViewSet):
    queryset = Cart.objects.all()
    serializer_class = CartSerializer

    """ Add to cart & Update quantity """

    @permission_classes([AllowAny])
    def post(self, request):
        item_id = request.POST.get('item')
        quantity = request.POST.get('quantity')
        variant = request.POST.get('variant')
        total_price = 0

        """Update quantity"""
        obj = Cart.objects.filter(item_id=item_id)
        if obj.exists():
            print("update quantity")
            for obj in obj:
                unit_price = obj.item.unit_price
            if quantity:
                if int(quantity) < 1:
                    obj = get_object_or_404(Cart, item_id=item_id)
                    obj.delete()
                    queryset = Cart.objects.all()
                    ser = self.serializer_class(queryset, many=True)
                    return Response({"data": ser.data}, status=status.HTTP_200_OK)

                total_price = (float(unit_price) * float(quantity))

                update_item = Cart.objects.get(item_id=item_id)

                """Update item contains variant """

                if update_item.variant:
                    total_price = (float(update_item.variant.vari_price) * float(quantity))

                obj = Cart.objects.filter(item_id=item_id)

                obj.update(quantity=quantity, item_total=total_price)
                queryset = Cart.objects.all()
                ser = self.serializer_class(queryset, many=True)
                return Response({'status': 'successfully updated quantity', "data": ser.data}, status.HTTP_200_OK)

        """Add to cart"""
        if item_id:
            item = Items.objects.get(id=item_id)
            total_price = (float(item.unit_price) * float(quantity))
            """If Item has variant """
            if variant:
                item_varia = ItemVariant.objects.get(id=variant)
                total_price = (float(item_varia.vari_price) * float(quantity))

        print("total", total_price)
        cart_obj = Cart.objects.create(item_id=item_id, quantity=quantity, item_total=total_price, variant_id=variant)
        serializer = self.serializer_class(cart_obj)
        return Response({'status': 'success', 'data': serializer.data}, status.HTTP_200_OK)

    """remove and list cart item"""

    @permission_classes([AllowAny])
    def get(self, request, *args, **kwargs):
        item_id = request.GET.get('item_id')
        # Remove cart item **************************
        if item_id:
            obj = get_object_or_404(Cart, item_id=item_id)
            obj.delete()
            return Response({'message': 'Item Removed Successfully'}, status.HTTP_200_OK)
        queryset = Cart.objects.all()
        serializer = self.serializer_class(queryset, many=True)
        return Response({'data': serializer.data}, status.HTTP_200_OK)


# Order

class OrderApiView(viewsets.ModelViewSet):
    permission_classes = (IsAuthenticated,)
    queryset = Order.objects.all()
    serializer_class = OrderSerializer
    order_item_serializer = OrderItemSerializer

    def post(self, request, *args, **kwargs):
        cart_items = Cart.objects.all()

        delivery_method_id = request.POST.get('delivery_method_id')
        payment_method_id = request.POST.get('payment_method_id')

        offer_id = request.POST.get('offer_id')

        if cart_items.exists():
            """Create Order"""
            order = Order.objects.create(customer=request.user)
            if order:
                for item in cart_items:
                    order_item = OrderItem.objects.create(order_id=order.id, item=item.item, quantity=item.quantity,
                                                          item_total=item.item_total, variant=item.variant)

                order_list = OrderItem.objects.filter(order_id=order.id)

                """Calculate grand total of the order and Update Order Table"""
                item_price = order_list.aggregate(Sum('item_total'))
                obj = Order.objects.filter(id=order.id)
                obj.update(grand_total=int(item_price['item_total__sum']), delivery_method=delivery_method_id,
                           payment_method=payment_method_id, order_status='pending')

                """If add any offers"""

                if offer_id:
                    offer = OfferCoupon.objects.get(id=offer_id, customer=request.user)
                    order_obj = Order.objects.get(id=order.id)

                    total_amt = order_obj.grand_total
                    offer_perc = offer.offers
                    amount_perc = (total_amt * offer_perc) / 100
                    grand_total = total_amt - amount_perc
                    obj.update(offer=offer_id, offer_included_price=grand_total)

                order_serializer = OrderSerializer(obj, many=True)
                item_serializer = OrderItemSerializer(order_list, many=True)

                """ Clear Cart Tabel """

                Cart.objects.all().delete()
                return Response({"message": "Order Confirm", "order data": order_serializer.data,
                                 "item data": item_serializer.data}, status=status.HTTP_200_OK)

        return Response({"Message": "Cart is empty!"}, status=status.HTTP_404_NOT_FOUND)

    def re_order(self, request, *args, **kwargs):
        order_id = request.POST.get('order_id')
        order_status = request.POST.get('order_status')

        """Re-Order"""
        if order_id and order_status == 'reorder':
            order = Order.objects.get(id=order_id)
            if order:
                order_items = OrderItem.objects.filter(order_id=order_id)
                print(order_items)
                for item in order_items:
                    cart_item = Cart(item_id=item.item.id, quantity=item.quantity, item_total=item.item_total)
                    cart_item.save()

                return Response({"message": "Reordered Successfully"}, status=status.HTTP_200_OK)

            return Response({"message": "Order not found"}, status=status.HTTP_404_NOT_FOUND)

    def get(self, request, *args, **kwargs):
        order_status = request.GET.get('order_status')
        order_id = request.GET.get('order_id')
        serializer = OrderItemSerializer

        """orders details"""
        if order_status == 'pending':
            conf_orders = OrderItem.objects.filter(order__order_status=order_status)
            serializer = serializer(conf_orders, many=True)
            return Response({"message": "confirm order", "data": serializer.data}, status=status.HTTP_200_OK)

        """History"""
        if order_status == 'done':
            done_orders = OrderItem.objects.filter(order__order_status=order_status)
            serializer = OrderItemSerializer(done_orders, many=True)
            print(serializer.data)
            return Response({"message": "Done orders", "data": serializer.data}, status=status.HTTP_200_OK)

        if order_id and order_status == 'cancel':
            obj = Order.objects.filter(id=order_id)
            obj.update(order_status='cancel')
            return Response({"message": "Cancel Order"}, status=status.HTTP_200_OK)

        if order_id:
            order_details = OrderItem.objects.filter(order__id=order_id)
            serializer = serializer(order_details, many=True)
            return Response({"message": "Order details", "data": serializer.data})

        return Response({"message": "Data not found!"}, status=status.HTTP_404_NOT_FOUND)


class DeliveryMethodApiView(viewsets.ModelViewSet):
    permission_classes = (IsAuthenticated,)
    queryset = DeliveryMethod.objects.all()
    serializer_class = DeliveryMethodSerializer

    def get(self, request):
        serializer = self.serializer_class(self.queryset, many=True)
        return Response({"message": "Delivery Method", "data": serializer.data}, status=status.HTTP_200_OK)


class SignOutApiView(viewsets.ModelViewSet):
    permission_classes = (IsAuthenticated,)
    serializer_class = SignOutSerializer

    def get(self, request):
        if request.user.is_authenticated:
            AccessToken.objects.filter(user_id=request.user.pk).update(expires=timezone.now())
            RefreshToken.objects.filter(user_id=request.user.pk).update(revoked=timezone.now())
            logout(request)
            return Response({"message": "logout successful"}, status=status.HTTP_200_OK)


class OffersApiView(viewsets.ModelViewSet):
    permission_classes = (IsAuthenticated,)
    serializer_class = OfferSerializer

    def get(self, request):
        print("offers item")
        obj = Items.objects.exclude(offer_price__isnull=True)
        ser = ItemSerializer(obj, many=True)
        return Response({"message": "Get offers item", "data": ser.data})


class FAQView(viewsets.ModelViewSet):
    permission_classes = (IsAuthenticated,)
    serializer_class = FAQAserializer
    queryset = FAQ.objects.all()

    def get(self, request):
        data = self.queryset
        ser = self.serializer_class(data, many=True)
        return Response({"Message": "FAQ", "data": ser.data}, status=status.HTTP_200_OK)


class PaymentMethodView(viewsets.ModelViewSet):
    permission_classes = (IsAuthenticated,)
    serializer_class = PaymentMethodSerializer
    queryset = PaymentMethod.objects.all()

    def get(self, request):
        ser = self.serializer_class(self.queryset, many=True)
        return Response({"message": "Payment methods", "data": ser.data}, status=status.HTTP_200_OK)


class UserCouponView(viewsets.ModelViewSet):
    permission_classes = (IsAuthenticated,)
    serializer_class = OfferCouponSerializer

    def get(self, request):
        user = request.user
        obj = OfferCoupon.objects.filter(customer=user)
        if obj:
            ser = self.serializer_class(obj, many=True)
            return Response({"message": "success", "data": ser.data}, status=status.HTTP_200_OK)
        return Response({"message": "No Offers found!"}, status=status.HTTP_404_NOT_FOUND)


""" Add multiple delivery address """


class DeliveryAddressView(viewsets.ModelViewSet):
    permission_classes = (IsAuthenticated,)
    queryset = DeliveryAddress.objects.all()
    serializer_class = DeliveryAddressSerializer

    @csrf_exempt
    def post(self, request, *args, **kwargs):
        print("post")
        customer = request.user
        house_name = request.POST.get('house_name')
        flat_no = request.POST.get('flat_no')
        landmark = request.POST.get('landmark')
        pin = request.POST.get('pin')
        city = request.POST.get('city')

        addr_obj = DeliveryAddress.objects.create(customer=customer, house_name=house_name, flat_no=flat_no,
                                                  landmark=landmark, pin=pin, city=city)
        if addr_obj:
            ser = self.serializer_class(addr_obj)
            return Response({"message": "Success", "data": ser.data}, status=status.HTTP_200_OK)

        return Response({"message": "Creation Failed!"})

    def get(self, request, *args, **kwargs):
        obj = DeliveryAddress.objects.filter(customer=request.user)
        ser = self.serializer_class(obj, many=True)
        return Response({"Message": "Address", "data": ser.data}, status=status.HTTP_200_OK)
