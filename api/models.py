import uuid
from datetime import date, datetime
from importlib.resources import _

from django.db import models
from django.contrib.auth.models import BaseUserManager, AbstractBaseUser, PermissionsMixin
from phonenumber_field.modelfields import PhoneNumberField
from versatileimagefield.fields import VersatileImageField,PPOIField
from api.validator import validate_possible_number




#
class PossiblePhoneNumberField(PhoneNumberField):
    """Less strict field for phone numbers written to database."""
    default_validators = [validate_possible_number]



class UserManager(BaseUserManager):

    def create_user(self, email, password=None,phone=None,**extra_fields):
        print("$$$")
        print(self)
        print("create user")
        print(*extra_fields)
        """
        Creates and saves a User with the given email and password.
        """

        if not email:
            raise ValueError('Users must have an email address')

        user = self.model(
            email=self.normalize_email(email),
            phone=phone,
            **extra_fields
        )

        user.set_password(password)
        user.save(using=self._db)
        return user


    def create_superuser(self, email, password, **extra_fields):
        """
        Creates and saves a superuser with the given email and password.
        """
        user = self.create_user(
            email,
            password=password,
        )
        user.is_staff = True
        user.is_superuser = True
        user.is_admin = True
        user.save(using=self._db)
        return user





class User(AbstractBaseUser, PermissionsMixin):
    first_name = models.CharField(max_length=50)
    last_name = models.CharField(max_length=50)
    email = models.EmailField(verbose_name='email address',
                              max_length=255,
                              unique=True)
    address = models.TextField(null=True, blank=True)
    pin = models.IntegerField(null=True, blank=True)
    landmark = models.CharField(max_length=200, null=True, blank=True)
    phone = PossiblePhoneNumberField(_('phone_number'), db_index=True, unique=True,blank=True,null=True)
    password = models.CharField(max_length=225)
    is_active = models.BooleanField(default=True)
    is_staff = models.BooleanField(default=True)
    is_admin = models.BooleanField(default=False)

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = ['password', 'phone']  # Email & Password are required by default.

    objects = UserManager()



class FoodType(models.Model):
    food_type = models.CharField(max_length=200)

    def __str__(self):
        return self.food_type

class FoodCategory(models.Model):
    food_category = models.CharField(max_length=300)

    def __str__(self):
        return self.food_category
#
FOOD_STATUS = (
    ('available','available'),
    ('not-available','not available'),
)

class Image(models.Model):
    name = models.CharField(max_length=225)
    image = VersatileImageField('Image',upload_to='images/',ppoi_field='image_ppoi')

    image_ppoi = PPOIField()

    def __str__(self):
        return self.name

class Items(models.Model):
    name = models.CharField(max_length=300)
    food_type = models.ForeignKey(FoodType,on_delete=models.CASCADE)
    food_category = models.ForeignKey(FoodCategory,on_delete=models.CASCADE)
    # image = models.ManyToManyField('api.Image',related_name='items')
    slug = models.SlugField(max_length=300,blank=True,null=True)
    description = models.TextField(blank=True,null=True)
    unit_price = models.FloatField(blank=True,null=True,default=0)
    quantity = models.IntegerField(blank=True,null=True)
    item_rating = models.IntegerField(blank=True,null=True,default=1)
    food_status = models.CharField(max_length=500,blank=True,null=True, choices=FOOD_STATUS)
    offer_price = models.FloatField(null=True,blank=True)
    item_img = models.ForeignKey(Image,on_delete=models.CASCADE,blank=True,null=True)


    def __str__(self):
        return self.name



class ItemVariant(models.Model):
    item = models.ForeignKey(Items,on_delete=models.CASCADE)
    variant = models.CharField(max_length=100)
    vari_price = models.FloatField()

    def __str__(self):
        return self.item.name














class DeliveryMethod(models.Model):
    delivery_method = models.CharField(max_length=200,blank=True,null=True)

    def __str__(self):
        return self.delivery_method





class ItemReview(models.Model):
    review  = models.TextField()
    customer = models.ForeignKey(User,on_delete=models.CASCADE,blank=True,null=True)
    item = models.ForeignKey(Items,on_delete=models.CASCADE,blank=True,null=True)
    review_date = models.DateField(default=date.today)


    def __str__(self):
        return self.item.name



class Rating(models.Model):
    rating = models.IntegerField(default=0)
    item = models.ForeignKey(Items,on_delete=models.CASCADE)
    customer = models.ForeignKey(User,on_delete=models.CASCADE,null=True,blank=True)
    rating_date = models.DateField(default=date.today)


    def __str__(self):
        return self.item.name




class OfferCoupon(models.Model):
    customer = models.ForeignKey(User,on_delete=models.CASCADE)
    offers = models.BigIntegerField()

    def __str__(self):
        return self.customer.email



class Cart(models.Model):
    item = models.ForeignKey(Items,related_name='cart',on_delete=models.CASCADE,null=True,blank=True)
    quantity = models.IntegerField(default=1)
    item_total = models.FloatField()
    variant = models.ForeignKey(ItemVariant,on_delete=models.CASCADE,null=True,blank=True)
    customer = models.ForeignKey(User,on_delete=models.CASCADE,null=True,blank=True)

    # def __str__(self):
    #     return self.item.name


ORDER_STATUS = (
    ('pending','pending'),
    ('confirm','confirm'),
    ('done','done'),
    ('reorder','reorder'),
)

PAYMENT_STATUS = (
    ('pending','pending'),
    ('cancel','cancel'),
    ('confirm','confirm'),
)



class PaymentMethod(models.Model):
    payment_method = models.CharField(max_length=500)

    def __str__(self):
        return self.payment_method


class DeliveryAddress(models.Model):
    customer = models.ForeignKey(User,on_delete=models.CASCADE)
    house_name  = models.CharField(max_length=1000)
    flat_no = models.CharField(max_length=200)
    landmark = models.CharField(max_length=500)
    pin = models.IntegerField()
    city = models.CharField(max_length=500)

    def __str__(self):
        return str(self.customer.email)


OFFER_STATUS = (
    ('available','available'),
    ('not_anailable','not available'),
    ('expired','expired'),
)


class OfferTable(models.Model):
    offer_label = models.CharField(max_length=1000)
    offer_price = models.BigIntegerField()
    offer_status = models.CharField(max_length=500,choices=OFFER_STATUS)
    expire_date = models.DateField(default=datetime.now(),blank=True,null=True)

    def __str__(self):
        return self.offer_label


class UserOffer(models.Model):
    offer = models.ForeignKey(OfferTable,on_delete=models.CASCADE)
    customer = models.ForeignKey(User,on_delete=models.CASCADE)

    def __str__(self):
        return str(self.customer.email)




class Order(models.Model):
    order_number = models.UUIDField(default=uuid.uuid4,unique=True,db_index=True,editable=False)
    customer = models.ForeignKey(User,on_delete=models.CASCADE)
    grand_total = models.FloatField(default=0)
    delivery_method = models.ForeignKey(DeliveryMethod,on_delete=models.CASCADE,blank=True,null=True)
    payment_method = models.ForeignKey(PaymentMethod,on_delete=models.CASCADE,blank=True,null=True)
    order_status = models.CharField(max_length=300,choices=ORDER_STATUS,default='pending')
    payment_status = models.CharField(max_length=300,choices=PAYMENT_STATUS,default='pending')
    order_date = models.DateTimeField(auto_now_add=True)
    offer = models.ForeignKey(OfferTable,on_delete=models.CASCADE,null=True,blank=True)
    offer_included_price = models.FloatField(blank=True,null=True)
    delivery_address = models.ForeignKey(DeliveryAddress,on_delete=models.CASCADE,blank=True,null=True)

    def __str__(self):
        return self.customer.first_name


class OrderItem(models.Model):
    order = models.ForeignKey(Order,on_delete=models.CASCADE)
    item = models.ForeignKey(Items,on_delete=models.CASCADE)
    quantity = models.IntegerField()
    item_total = models.FloatField()
    variant = models.ForeignKey(ItemVariant,on_delete=models.CASCADE,null=True,blank=True)

    def __str__(self):
        return self.item.name





class Offers(models.Model):
    offer = models.BigIntegerField()
    item = models.ManyToManyField('api.Items',related_name='offers')


class FAQ(models.Model):
    qus = models.TextField()
    ans = models.TextField()

    def __str__(self):
        return self.qus


