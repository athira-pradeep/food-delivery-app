import base64
from oauth2_provider.models import Application
from django.utils import timezone
from oauth2_provider.models import AccessToken, RefreshToken
from oauth2_provider.settings import oauth2_settings
import random, base64, datetime
from oauthlib import common
from oauth2_provider.settings import oauth2_settings
from django.conf import settings


def OauthClientIDANDSecret(application_id=None):
    application = Application.objects.get(pk=application_id)
    return {'client_id': application.client_id, 'client_secret': application.client_secret}


def GenerateOauthToken(request, user, application_id=None):
    application = Application.objects.get(client_id=application_id)
    expires = timezone.now() + datetime.timedelta(seconds=oauth2_settings.ACCESS_TOKEN_EXPIRE_SECONDS)
    scope = " ".join(settings.OAUTH2_PROVIDER['SCOPES'].keys())

    access_token = common.generate_token()
    refresh_token = common.generate_token()
    obj_access_token = AccessToken(
        user=user,
        scope=scope,
        expires=expires,
        token=access_token,
        application=application
    )
    obj_access_token.save()
    obj_refresh_token = RefreshToken(
        user=user,
        token=refresh_token,
        application=application,
        access_token=obj_access_token
    )
    obj_refresh_token.save()
    return {"access_token": access_token, "refresh_token": refresh_token, "expires_in": expires, scope: scope,
            "token_type": "Bearer"}

